How to play the game:

1. Once yo start the app and hapsim the game will start automaticly
2. Press True to answer if it the statement is True
3. Press False otherwise
4. The LED will turn on based on your answer, RA will be shown with green, WA with RED
5. Score will increase if RA, and won't increase if WA
6. Score will be updated everytime user finished answering question
7. The end of the game will contain user's last score
8. User can play again by pressing the button True once the game is finished
